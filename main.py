import sys

privilege = 'guest'
arguments = []

def get_login():
    while 1:
        username = raw_input('Enter username: ')
        if username.lower() == 'admin':
            privilege = 'admin'
            is_admin()
        elif username.lower() == 'user':
            privilege = 'user'
            is_user()
        else:
            privilege = 'guest'
            print 'Invalid username.'


def check_availability(bookShowID):
    print 'Checking availability'

    counter = 0

    with open('transactions', 'r') as t:
        next(t)


        for line in t:
            transaction = line.split('|')
            transactionID = transaction[0]
            transactionShowID = transaction[1]
            if transactionShowID == bookShowID:
                counter += 1
    t.close()







    with open('shows', 'r') as s:
        next(s)

        for line in s:
            show = line.split('|')
            showSeats = show[4]
            showID = show[0]
            if showID == bookShowID:
                if int(showSeats) > counter:
                    return True
                else:
                    return False
    s.close()


def view_shows():
    print 'Viewing shows'

    with open('shows', 'r') as s:
        next(s)
        for line in s:
            show = line.split('|')
            showID = show[0]
            showScreen = show[1]
            showTime = show[2]
            showMovieName = show[3]
            showSeats = show[4]
            showCost = show[5]

            print '---'
            print 'Show ID = ' + showID
            print 'Show Screen = ' + showScreen
            print 'Show Time = ' + showTime
            print 'Show Movie Name = ' + showMovieName
            print 'Show Seats = ' + showSeats
            print 'Show Cost = ' + showCost

    s.close()


def add_show():
    print 'Adding show'

    with open('shows', 'r') as s:
        for line in s:
            last = line

    s.close()

    last = last.split('|')
    lastID = int(last[0])
    newID = lastID + 1
    newID = str(newID)
    newScreen = raw_input('Enter Screen: ')
    newTime = raw_input('Enter Show Time: ')
    newMovieName = raw_input('Enter Movie Name: ')
    newSeats = raw_input('Enter Seats: ')
    newCost = raw_input('Enter Cost: ')

    newShow = newID + '|' + newScreen + '|' + newTime + '|' + newMovieName + '|' + newSeats + '|' + newCost

    with open('shows', 'a') as s:
        s.write(newShow)
    s.close()

def delete_show():
    print 'Delete show'
    view_shows()
    deleteShowID = raw_input('Enter ID of show to delete: ')

    with open('shows', 'r') as s:
        next(s)
        allShows = s.readlines()
    s.close()

    with open('shows', 'w') as s:
        for line in allShows:
            show = line.split('|')
            showID = int(show[0])
            if showID != deleteShowID:
                s.write(line)

    s.close()

def book_ticket():
    print 'Booking ticket'
    view_shows()
    bookShowID = raw_input('Enter ID of show to book: ')
    if check_availability(bookShowID):
        numberOfSeats = raw_input('Enter number of seats you want to book: ')
    else:
        print 'Show is booked out. Please try another show.'

def is_admin():
    print 'Welcome, Admin!'
    option = None

    while 1:
        print 'What do you want to do?'
        print '[V]iew Shows \t [A]dd Show \t [D]elete Show \t [E]xit'
        option = raw_input('Your choice: ')

        if option.lower() == 'v' or option.lower() == 'view':
            view_shows()
        elif option.lower() == 'a' or option.lower() == 'add':
            add_show()
        elif option.lower() == 'd' or option.lower() == 'delete':
            delete_show()
        elif option.lower() == 'e' or option.lower() == 'exit':
            sys.exit('Logged out successfully.')
        else:
            print 'Invalid option.'


def is_user():
    print 'Welcome, User!'

    while 1:
        print 'What do you want to do?'
        print '[V]iew Shows \t [B]ook Tickets \t [E]xit'
        option = raw_input('Your choice: ')

        if option.lower() == 'v' or option.lower() == 'view':
            view_shows()
        elif option.lower() == 'b' or option.lower() == 'book':
            book_ticket()
        elif option.lower() == 'e' or option.lower() == 'exit':
            sys.exit('Logged out successfully.')
        else:
            print 'Invalid option.'


def redirect(arguments):
    
    command = arguments[1].lower()

    if command == '-login':
        username = arguments[2].lower()
        if username == 'admin':
            privilege = 'admin'
            is_admin()
        elif username == 'user':
            privilege = 'user'
            is_user()
        else:
            privilege = 'guest'
            print 'Error logging in. Invalid username.'

for line in sys.argv:
    arguments.append(line);

if len(arguments) == 3:
    redirect(arguments)
else:
    get_login()
